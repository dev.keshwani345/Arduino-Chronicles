int ledPin=3;
String sos="sos";
int dotSpeed=250;
int dashSpeed=700;
int soslen=sos.length();

void setup() {
  Serial.begin(9600);
  pinMode(3,OUTPUT);
  Serial.println("in");
}

void loop() {
  for(int mg=0;mg<soslen;mg++){
    if((String)sos[mg]=="s"){
      for(int i=0;i<soslen;i++){
        digitalWrite(ledPin,HIGH);
        delay(dotSpeed);
        digitalWrite(ledPin,LOW);
        delay(dotSpeed);
      }
    }
    else{
      for(int i=0;i<soslen;i++){
        digitalWrite(ledPin,HIGH);
        delay(dashSpeed);
        digitalWrite(ledPin,LOW);
        delay(dashSpeed);
      }
    }
  }
  delay(1100);
}
