void setup() {
  pinMode(8,OUTPUT);//YELLOW
  pinMode(7,OUTPUT);//RED
  pinMode(11,OUTPUT);//GREEN
}

void loop() {
  digitalWrite(7,HIGH);
  delay(7000);
  digitalWrite(7,LOW);
  delay(500);
  digitalWrite(8,HIGH);
  delay(2000);
  digitalWrite(8,LOW);
  delay(500);
  digitalWrite(11,HIGH);
  delay(10000);
  digitalWrite(11,LOW);
}
